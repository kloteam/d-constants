import countries from "./src/countries";

const basic = {
  MAIN: "main",
  KIND: "kind",
  DESC_LIST: "description",
  DESC_TAGS: "tags",
  FEEL: "predefind",
  DREAM: "dream",
  LIFE: "life",
  FINGERPRINT: "f",
  TEST: "test",
  GEO: "geo",
  TIME: "time",
  EXCLUDE_EVENT: "exclude_public_posting",
  GEO_PARTS: {
    NAME: "country_name",
    CODE: "country_code"
  },
  EXACT: "exact",
  GLOBAL: "global",
  DREAM_STORY: "dream_story",
  DREAM_MESSAGE: "dream_message"
};

const mainEvents = [
  {
    name: "dream",
    label: "Dream",
    realm: basic.MAIN
  },
  {
    name: "life",
    label: "Real Life Event",
    realm: basic.MAIN
  }
];

const mainEventServerObject = () => {
  const out = {};
  mainEvents.forEach(e => {
    out[e.name.toUpperCase()] = e.name;
  });
  return out;
};

const predefinedFeel = [
  {
    name: "good",
    label: "Good",
    realm: basic.FEEL
  },
  {
    name: "bad",
    label: "Bad",
    realm: basic.FEEL
  },
  {
    name: "other",
    label: "Other",
    realm: basic.FEEL
  }
];
const lifeFeel = predefinedFeel.concat([
  {
    name: "strange",
    label: "Strange",
    realm: basic.FEEL
  }
]);

const lifeFeelServerObject = () => {
  const out = {};
  lifeFeel.forEach(e => {
    out[e.name.toUpperCase()] = e.name;
  });
  return out;
};

const dreamFeel = predefinedFeel.concat([
  {
    name: "strange",
    label: "Strange",
    realm: basic.FEEL
  }
]);

const dreamFeelServerObject = () => {
  const out = {};
  dreamFeel.forEach(e => {
    out[e.name.toUpperCase()] = e.name;
  });
  return out;
};

const dreamFeelArray = () => {
  const out = [];
  dreamFeel.forEach(e => {
    out.push(e.name);
  });
  return out;
};

const descriptionCommmon = [
  {
    name: "relatives",
    label: "Family and relatives",
    realm: basic.DESC_LIST
  },
  {
    name: "friends",
    label: "Friends and acquitences",
    realm: basic.DESC_LIST
  },
  {
    name: "job_finance",
    label: "Job/ Finance",
    realm: basic.DESC_LIST
  },
  {
    name: "health",
    label: "Health",
    realm: basic.DESC_LIST
  },
  {
    name: "death",
    label: "Birth",
    realm: basic.DESC_LIST
  },
  {
    name: "birth",
    label: "Death",
    realm: basic.DESC_LIST
  }
];

const lifeDescList = descriptionCommmon.concat([
  {
    name: "area",
    label: "In my area",
    realm: basic.DESC_LIST
  }
]);

const dreamDescList = descriptionCommmon.concat([
  {
    name: "animals",
    label: "Animal/ Insect",
    realm: basic.DESC_LIST
  },
  {
    name: "medium",
    label: "Water/ Flying",
    realm: basic.DESC_LIST
  },
  {
    name: "self",
    label: "Self",
    realm: basic.DESC_LIST
  },
  {
    name: "lucid",
    label: "Lucid dream",
    realm: basic.DESC_LIST
  },
  {
    name: "vivid",
    label: "Vivid dream",
    realm: basic.DESC_LIST
  },
  {
    name: "astral",
    label: "Astral projecton",
    realm: basic.DESC_LIST
  }
]);

const lifeDescListServer = () => {
  const out = {};
  lifeDescList.forEach(e => {
    out[e.name.toUpperCase()] = e.name;
  });
  return out;
};

const dreamDescListServer = () => {
  const out = {};
  dreamDescList.forEach(e => {
    out[e.name.toUpperCase()] = e.name;
  });
  return out;
};

const lifeDescListServerArray = () => lifeDescList.map(e => e.name);
const dreamDescListServerArray = () => dreamDescList.map(e => e.name);

const second = 1000;
const minute = second * 60;
const hour = minute * 60;
const hours = {
  "1h": hour,
  "2h": 2 * hour,
  "4h": 4 * hour,
  "12h": 12 * hour
};
const days = {
  "1day": 24 * hour,
  "2day": 48 * hour,
  "30day": 30 * 24 * hour
};

const timeConstants = { ...hours, ...days };

module.exports = {
  lifeDescListSA: lifeDescListServerArray(),
  dreamDescListSA: dreamDescListServerArray(),
  dreamDescList,
  lifeDescList,
  dreamFeelSO: dreamFeelServerObject(),
  lifeFeelSO: lifeFeelServerObject(),
  mainEvents,
  lifeFeel,
  dreamFeel,
  basic,
  countries: countries,
  timeConstants
};
/**
 * @returns {Array} - [relatives, animals,...]
 */
module.exports.LifeDescListSA = lifeDescListServerArray();

/**
 * @returns {Array} - [relatives, animals,...]
 */
module.exports.DreamDescListSA = dreamDescListServerArray();

/**
 * @returns {Array} - [{name: 'relatives',label: "Family and relatives",realm: basic.DESC_LIST},{...},...]
 */
module.exports.DreamDescList = dreamDescList;

/**
 * @returns {Array} - [{name: 'relatives',label: "Family and relatives",realm: basic.DESC_LIST},{...},...]
 */
module.exports.LifeDescList = lifeDescList;

/**
 * @returns {Object} - {GOOD: 'good', BAD: 'bad'}
 */
module.exports.DreamFeelSO = dreamFeelServerObject();

/**
 * @returns {Object} - {GOOD: 'good', BAD: 'bad'}
 */
module.exports.LifeFeelSO = lifeFeelServerObject();

/**
 * @returns {Array} - ['good', 'bad',...]
 */
module.exports.dreamFeelArray = dreamFeelArray();

/**
 * @returns {Array} - [{name: "dream", label: "Dream",realm: basic.MAIN},...]
 */
module.exports.MainEvents = mainEvents;

/**
 * @returns {Array}
 * [{
    name: "good",
    label: "Good",
    realm: basic.FEEL
  },...]
 */
module.exports.LifeFeel = lifeFeel;

/**
 * @returns {Array}
 * [{
    name: "good",
    label: "Good",
    realm: basic.FEEL
  },...]
 */
module.exports.DreamFeel = dreamFeel;

/**
 * @returns {Object}
  MAIN: "main",
  KIND: "kind",
  DESC_LIST: "description",
  DESC_TAGS: "tags",
  FEEL: "predefind",
  DREAM: "dream",
  LIFE: "life",
  FINGERPRINT: "f",
  TEST: "test",
  GEO: "geo"
 */
module.exports.Basic = basic;

/**
 * @returns {Array}
 * [{
    country_code: "AF",
    country_name: "Afghanistan",
    dialling_code: "+93"
  },...],
 */
module.exports.Countries = countries;

module.exports.timeConstants = timeConstants;
module.exports.hours = hours;
module.exports.days = days;
